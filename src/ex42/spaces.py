# -*- coding: utf-8 -*-
'''
Created on Feb 17, 2013

@author: Lê Ngọc Minh
'''
from ex42 import ukwac
from ex42.ukwac import lemma, sentences, dependency_nodes
from logging import getLogger
from string import letters, uppercase
import locale
import os


__log = getLogger("ex42.spaces")


def __extract_domain_column_of_direction(nodes, index, direction, columns):
    i = index + direction
    count = 0;
    while count < 7 and i in range(len(nodes)):
        node = nodes[i]
        # an error in UkWaC: most % is marked as NN
        if ukwac.is_noun(node):
            # word form is used in the original paper, see table 1&2
            columns.append(node.word.lower()) 
            return 
        if node.pos[0] in letters:
            count += 1
        i += direction 


def extract_domain_columns(nodes, index):
    columns = []
    __extract_domain_column_of_direction(nodes, index, 1, columns)
    __extract_domain_column_of_direction(nodes, index, -1, columns)
    if len(columns) == 2 and columns[0] == columns[1]:
        return columns[1:] # remove one context if they are the same
    return columns


def __strip_pos(pattern):
    while len(pattern) > 0 and pattern[-1][0] in uppercase:
        del pattern[-1]


def __replace_T_by_to(pattern):
    for index, item in enumerate(pattern):
        if item == "T":
            pattern[index] = "to"


def __reduce_noun_sequence(pattern):
    i = 0
    while i < len(pattern):
        if pattern[i] == "N":
            while i+1 < len(pattern) and pattern[i+1] == "N":
                del pattern[i+1]
        i += 1


def __extract_function_words_of_direction(nodes, index, direction):
    i = index + direction
    count = 0;
    has_verb = False
    general_pattern = []
    specific_pattern = []
    while count < 3 and i in range(len(nodes)):
        node = nodes[i]
        if node.pos[0] not in letters:
            break
        pos = node.pos[0]
        # general patterns
        if pos == "V":
            general_pattern.append(node.word.lower())
        else:
            general_pattern.append(pos)
        # specific patterns
        if pos == "V" or pos == "M" or pos == "I" or pos == "T":
            specific_pattern.append(node.word.lower())
        else:
            specific_pattern.append(pos)
        # has verb?
        if pos == "V":
            has_verb = True
        # next word
        count += 1
        i += direction
    __strip_pos(general_pattern)
    __strip_pos(specific_pattern)
    __replace_T_by_to(general_pattern)
    __replace_T_by_to(specific_pattern)
    __reduce_noun_sequence(general_pattern)
    __reduce_noun_sequence(specific_pattern)
    if direction < 0:
        general_pattern.reverse()
        specific_pattern.reverse()
    return (general_pattern, specific_pattern, has_verb)
        

def __combine_function_words(left, left_has_verb, right, right_has_verb, columns):
    if left and right:
        column = []
        column.extend(left)
        column.append('X')
        column.extend(right)
        columns.append("_".join(column))
    if left_has_verb:
        columns.append("_".join(left) + "_X")
    if right_has_verb:
        columns.append("X_" + "_".join(right))


def extract_function_columns(nodes, index):
    columns = []
    right = __extract_function_words_of_direction(nodes, index, 1)
    left = __extract_function_words_of_direction(nodes, index, -1)
    __combine_function_words(left[0], left[2], right[0], right[2], columns)
    __combine_function_words(left[1], left[2], right[1], right[2], columns)
    return set(columns) # prevent duplication


def __general_coocurrences(files, callback,
                        filter_row_node, extract_row,
                        filter_column, extract_columns,
                        suspend_exceptions=False):
    count = 0
    for sentence in sentences(files):
        try:
            count += 1
            if count % 100000 == 0:
                __log.debug("Number of sentences of process %d so far: %s" 
                            %(os.getpid(), 
                            locale.format("%d", count, grouping=True)))
            nodes = dependency_nodes(sentence, relation_parsed=False)
            for i in range(len(nodes)):
                    node = nodes[i]
                    if not filter_row_node(node):
                        continue
                    row = extract_row(node)
                    columns = extract_columns(nodes, i)
                    for column in columns:
                        if not filter_column(column):
                            continue
                        try:
                            callback((row, column))
                        except StopIteration:
                            return
        except Exception as e:
            if suspend_exceptions:
                __log.error("Suspend an exception while processing %s. "
                            "The problematic sentence is dropped." %str(files))
                __log.exception(e)
            else:
                raise
    __log.debug("Processed sentences: %d" %count)


def domain_coocurrences(files, callback,
                        filter_row_node=lambda node: True, 
                        extract_row=lemma,
                        filter_column=lambda node: True,
                        suspend_exceptions=False):
    __general_coocurrences(files, callback, 
                           filter_row_node, extract_row,
                           filter_column, extract_domain_columns,
                           suspend_exceptions)
                
                
def function_coocurrences(files, callback,
                        filter_row_node=lambda node: True, 
                        extract_row=lemma,
                        filter_column=lambda node: True,
                        suspend_exceptions=False):
    __general_coocurrences(files, callback, 
                           filter_row_node, extract_row,
                           filter_column, extract_function_columns,
                           suspend_exceptions)


def __read_stripped_lines(path):
    with open(path, "r") as f:
        for line in f:
            yield line.strip()


def evaluate_contexts(contexts, ref_contexts):
    if isinstance(contexts, basestring):
        contexts_path = contexts
        contexts = set()
        with open(contexts_path, "r") as f:
            for line in f:
                context = line.strip().split("-")[0]
                contexts.add(context)
    if isinstance(ref_contexts, basestring):
        ref_contexts = set(__read_stripped_lines(ref_contexts))
    print len(contexts)
    print len(ref_contexts)
    correct = 0
    for context in contexts:
        if context in ref_contexts:
            correct += 1
    precision = correct / float(len(contexts))
    recall = correct / float(len(ref_contexts))
    f1 = precision * recall / (2*(precision + recall))
    return (precision, recall, f1)


def write_sparse(coocurrences, path, gzip=False):
    f = None
    if path.endswith(".gz"):
        f = gzip.open(path, "w")
    else:
        f = open(path, "w")
    with f:
        print len(coocurrences)
        for key in coocurrences:
            f.write("%s\t%s\t%d\n" %(key[0], key[1], coocurrences[key]))


def write(coocurrences, path, format_=None, gzip=False):
    if format_ is None:
        dot = path.rfind(".")
        format_ = path[dot+1:]
        if format_.endswith(".gz"):
            format_ = format[:-3]
    if format_ == 'sm':
        write_sparse(coocurrences, path, gzip)
    else:
        raise NotImplementedError
