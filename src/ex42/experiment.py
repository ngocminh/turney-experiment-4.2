# -*- coding: utf-8 -*-
'''
Created on Feb 17, 2013

@author: Lê Ngọc Minh
'''
from ConfigParser import ConfigParser, DEFAULTSECT
from collections import Counter
from composes.composition.multiplicative import Multiplicative
from composes.semantic_space.space import Space
from composes.similarity.cos import CosSimilarity
from composes.transformation.dim_reduction.svd import Svd
from composes.transformation.scaling.ppmi_weighting import PpmiWeighting
from composes.utils import io_utils
from ex42 import ukwac, spaces, Question
from ex42.data import merge, read_counts, write_counter
from ex42.ukwac import node2str
from glob import glob
from heapq import nlargest
from logging import DEBUG
from logging.config import fileConfig
from multiprocessing import Pool, Process
import locale
import logging
import multiprocessing
import os
import subprocess
import sys
import threading


__log = logging.getLogger('ex42')
__measure = CosSimilarity()
try:
    locale.setlocale(locale.LC_ALL, 'en_US') # enable pretty number printing
except:
    try:
        locale.setlocale(locale.LC_ALL, 'en_US.UTF-8') # enable pretty number printing
    except:
        pass
    

def __safe_write(path, func, *args):
    if not path:
        return
    temp_path = path + ".tmp"
    func(temp_path, *args)
    os.rename(temp_path, path)


def read_stripped_lines(path):
    with open(path, "r") as f:
        for line in f:
            yield line.strip()
            

def __write_contexts(path, count_tuples):
    with open(path, "w") as f:
        for t in count_tuples:
            f.write(t[0])
            f.write("\n")


def _count_contexts(iterate_coocurrences, corpus_path, words, threshold=0, 
                     multiprocessing=False, suspend_exceptions=True):
    counter = Counter()
    try:
        def increase_counter(c): counter[c[1]] += 1
        iterate_coocurrences(corpus_path, increase_counter, 
                             lambda node : not words or node2str(node) in words,
                             suspend_exceptions=suspend_exceptions)
        if threshold > 0:
            original_count = len(counter)
            deleted_count = 0
            for key in counter.keys():
                if counter[key] <= threshold:
                    del counter[key]
                    deleted_count += 1
            if __log.isEnabledFor(DEBUG):
                deleted_percent = deleted_count/float(original_count)*100
                __log.debug("Deleted %d (%.2f%%) low-frequency contexts from %s" 
                            %(deleted_count, deleted_percent, corpus_path))
        __log.debug("Number of keys from %s (process %s, thread %s): %d" 
                    %(corpus_path, os.getpid(), threading.current_thread().name, 
                      len(counter)))
    except Exception as e:
        if multiprocessing:
            __log.exception(e)
        else:
            raise
    return counter


def _count_and_write_contexts(dest, iterate_coocurrences, corpus_path, words, 
                              threshold=0, multiprocessing=False, 
                              suspend_exceptions=True):
    counter = _count_contexts(iterate_coocurrences, corpus_path, words, 
                              threshold, multiprocessing, suspend_exceptions)
    write_counter(counter, dest)
    __log.debug("Written to %s" %dest)
    return dest
    

def __debug_most_common_contexts(most_commons):
    if __log.isEnabledFor(DEBUG):
        __log.debug("20 first most common contexts with counts: %s", str(most_commons[:20]))
        __log.debug("20 last most common contexts with counts: %s", str(most_commons[-20:]))


def find_contexts_sequential(iterate_coocurrences, corpus_paths, words, n, 
                             result_path=None):
    '''
    Find contexts concurrently on multiple CPUs.

    words is the set of accepted words to find contexts for. Set to None to 
    disable filtering words.
    result_path is the path to store results to or load results from if it 
    already exists (hopefully from previous run of the same configuration).
    '''
    if result_path and os.path.exists(result_path):
        return set(read_stripped_lines(result_path))

    counter = Counter()
    if isinstance(corpus_paths, basestring):
        corpus_paths = (corpus_paths,)
    for path in corpus_paths:
        def increase_counter(c): counter[c[1]] += 1
        iterate_coocurrences(path, increase_counter,
                             lambda node : not words or node2str(node) in words,
                             suspend_exceptions=True)

    most_commons = counter.most_common(n)
    contexts = set(entry[0] for entry in most_commons)
    __safe_write(result_path, __write_contexts, most_commons)
    __debug_most_common_contexts(most_commons)
    __log.info("Finished counting contexts")
    return contexts


def _read_most_common_contexts(path, n):
    return nlargest(n, read_counts(path), key=lambda c: c[1])


END_OF_QUEUE = "__END_OF_QUEUE__" 

    
def add_more_paths_if_possible(queue, paths):
    while not queue.empty():
        part_path = queue.get_nowait()
        if part_path and part_path != END_OF_QUEUE:
            if __log.isEnabledFor(DEBUG):
                __log.debug("Additional file for better performance: %s" %part_path)
            paths.append(part_path)
        else:
            if part_path == END_OF_QUEUE:
                __log.debug("End of queue flag is put back")
                queue.put(part_path)
            break


def merge_parts(result_path, queue, timeout=300):
    merge_count = 0
    paths = [queue.get(timeout)]
    if not paths[0]: return
    part_path = queue.get(timeout)
    while part_path != END_OF_QUEUE:
        if part_path:
            paths.append(part_path)
            add_more_paths_if_possible(queue, paths)
            merge_path = result_path + ".merge-" + str(merge_count)
            if __log.isEnabledFor(DEBUG):
                __log.debug("Merging %s into %s" %(str(paths), merge_path))
            merge(merge_path, paths)
            paths = [merge_path]
            merge_count += 1
        part_path = queue.get(timeout)
    if __log.isEnabledFor(DEBUG):
        __log.debug("Paths before returning: " + str(paths))
    queue.put(paths[0])


def find_contexts_by_merging(iterate_coocurrences, corpus_paths, words, n, 
                             result_path=None, threshold=0, max_processes=2):
    '''
    Find contexts concurrently on multiple CPUs. To ensure scalability, this
    method use Linux sort command to merge partial results.

    words is the set of accepted words to find contexts for. Set to None to 
    disable filtering words.
    result_path is the path to store results to or load results from if it 
    already exists (hopefully from previous run of the same configuration).
    '''
    if result_path and os.path.exists(result_path):
        return set(read_stripped_lines(result_path))

    if __log.isEnabledFor(DEBUG):
        __log.debug("Max processes being used: " + str(max_processes))
        __log.debug("Start counting contexts")
    pool = Pool(max_processes)
    if isinstance(corpus_paths, basestring):
        corpus_paths = (corpus_paths,)
    
    queue = multiprocessing.Queue(len(corpus_paths))
    for index, path in enumerate(corpus_paths):
        part_path = result_path + ".part-" + str(index) 
        pool.apply_async(_count_and_write_contexts, 
                         args=(part_path, iterate_coocurrences, path, 
                               words, threshold),
                         kwds={"multiprocessing": True, 
                               "suspend_exceptions":True},
                         callback=lambda dest: queue.put(dest))
    pool.close()

    merge_process = Process(target=merge_parts, args=(result_path, queue))
    merge_process.start()
    
    pool.join()
    queue.put(END_OF_QUEUE)
    merge_process.join()

    most_commons = _read_most_common_contexts(queue.get_nowait(), n)
    __safe_write(result_path, __write_contexts, most_commons)
    __debug_most_common_contexts(most_commons)
    __log.info("Finished counting contexts")
    return set(item[0] for item in most_commons)


def _count_coocurrences(coocurrences_callback, corpus_paths, words, contexts, 
                       multiprocessing=False, suspend_exceptions=True):
    counter = Counter()
    try:
        def increase_counter(c): counter[c] += 1
        coocurrences_callback(corpus_paths, increase_counter,
                              lambda node : not words or node2str(node) in words, 
                              node2str, 
                              lambda column: not contexts or column in contexts,
                              suspend_exceptions)
        if __log.isEnabledFor(DEBUG):
            __log.debug("Number of keys from %s: %d" 
                        %(corpus_paths, len(counter)))
    except Exception as e:
        if multiprocessing:
            __log.error("Uncautch exception in process. The processing of %s "
                        "is terminated but the process is alive" %str(corpus_paths))
            __log.exception(e)
        else:
            raise
    return counter


def _count_and_write_coocurrences(dest, coocurrences_callback, corpus_paths, 
                                  words, contexts, 
                                  multiprocessing=False, suspend_exceptions=True):
    counter = _count_coocurrences(coocurrences_callback, corpus_paths, words, 
                                  contexts, multiprocessing, suspend_exceptions)
    write_counter(counter, dest)
    return dest
    

def count_coocurrences_sequential(coocurrences_callback, corpus_paths, 
                                  words, contexts, result_path):
    '''
    Find co-occurrences concurrently on multiple CPUs.
    '''
    if result_path and os.path.exists(result_path):
        return 

    counter = Counter()
    if isinstance(corpus_paths, basestring):
        corpus_paths = (corpus_paths,)
    for path in corpus_paths:
        def increase_counter(c): counter[c] += 1
        coocurrences_callback(path, increase_counter,
                              lambda node : not words or node2str(node) in words, 
                              node2str, 
                              lambda column: not contexts or column in contexts,
                              suspend_exceptions=True)
    
    def write(path): spaces.write_sparse(counter, path, gzip=True)
    __safe_write(result_path, write)
    if __log.isEnabledFor(DEBUG):
        __log.debug("20 most co-occurrences: " + str(counter.most_common(20)))
        __log.debug("Finished counting co-occurrences")
    return counter
    

def count_coocurrences_by_merging(coocurrences_callback, corpus_paths, 
                                       words, contexts, result_path, max_processes=2):
    '''
    Find co-occurrences concurrently on multiple CPUs.
    '''
    if result_path and os.path.exists(result_path):
        return 

    __log.debug("Max processes being used: " + str(max_processes))
    __log.debug("Start counting co-occurrences")
    
    queue = multiprocessing.Queue(len(corpus_paths))
    pool = Pool(max_processes)
    if isinstance(corpus_paths, basestring):
        corpus_paths = (corpus_paths,)
    for index, path in enumerate(corpus_paths):
        part_path = result_path + ".part-" + str(index)
        pool.apply_async(_count_and_write_coocurrences, 
                         args=(part_path, coocurrences_callback, path, words, contexts, True),
                         callback=lambda dest: queue.put(dest))
    pool.close()
    
    merge_process = Process(target=merge_parts, args=(result_path, queue))
    merge_process.start()
    
    pool.join()
    queue.put(END_OF_QUEUE)
    merge_process.join()
    
    os.rename(queue.get(), result_path)


def build_space(space_path, col_path, result_path):
    __log.debug("Start building semantic space")
    if result_path and os.path.exists(result_path):
        return io_utils.load(result_path)
    
    __log.debug("Start reading")
    space = Space.build(format="sm",
                        data=space_path,
                        cols=col_path)
    __log.debug("Finished reading")
    
    __log.debug("Start PPMI")
    space = space.apply(PpmiWeighting())
    if __log.isEnabledFor(DEBUG):
        __log.debug("Finished PPMI")
        __log.debug("Number of elements after PPMI: %d" 
                    % len(space.get_cooccurrence_matrix().mat.data))
        
    __log.debug("Start SVD")
    space = space.apply(Svd(1500))
    if __log.isEnabledFor(DEBUG):
        __log.debug("Finished SVD")
        __log.debug("Number of elements after SVD: %d" 
                    % len(space.get_cooccurrence_matrix().mat.data))
        
    def write(path): io_utils.save(space, path)
    __safe_write(result_path, write)
    __log.debug("Finished building semantic space")
    return space


def __convert_paths(s):
    paths = []
    for pattern in s.split(","):
        paths.extend(glob(pattern))
    return paths


def __index_of_max(values):
    '''
    Return the index of the first max value in provided values. 
    '''
    index = -1
    max_value = -1
    for i in range(len(values)):
        if values[i] > max_value:
            index = i
            max_value = values[i]
    return index


def build_composition_space(space, composition_model, questions):
    __log.debug("Start building compositional space.")
    if isinstance(questions, basestring):
        questions = Question.from_file(questions)
    composed_space = composition_model.compose(
            [(question.modifier, question.head, question.question)
            for question in questions], space)
    stacked_space = space.vstack(composed_space, space)
    __log.debug("Finished building compositional space.")
    return stacked_space


def answer(stacked_space, question, constrained=True):
    # compute similarity only for choices with a != c and b != c
    if constrained:
        choices = list([choice for choice in question.choices 
                        if choice != question.head and choice != question.modifier])
    else:
        choices = question.choices
    sims = [stacked_space.get_sim(question.question, choice, __measure)
            for choice in choices]
    index = max(range(len(sims)), key=lambda i: sims[i])
    ans = choices[index]
    if sims[index] <= 0:
        __log.warn('Negative or zero similarity answer: %s, of question: %s, '
                   'similarity scores: %s, choices: %s' 
                   %(ans, str(question), str(sims), str(choices)))
    return ans


def answer_and_score(stacked_space, questions, name=""):
    __log.debug("Start answering and scoring.")
    correct = 0
    for question in questions:
        ans = answer(stacked_space, question)
        if ans == question.correct_answer:
            correct += 1
        else:
            if __log.isEnabledFor(DEBUG):
                __log.debug("Wrong answer: %s of question: %s, choices: %s." % 
                            (ans, str(question), str(question.choices)))
    precision = correct/float(len(questions))
    print("Precision(%s): %f" %(name, precision))
    return precision


def run_turney_on_space(questions, name, raw_space, cols, processed_space):
    space = build_space(raw_space, cols, processed_space)
    answer_and_score(build_composition_space(space, Multiplicative(), questions),
                     questions, "%s, multiplicative" %name)


def __extract_words(questions):
    words = set()
    for question in questions:
        words.add(question.modifier)
        words.add(question.head)
        for foil in question.foils:
            words.add(foil)
    return words


def run_turney_sequential(conf):
    corpus_paths = __convert_paths(conf.get(DEFAULTSECT, "corpora"))
    if len(corpus_paths) <= 0:
        print "No corpus path is provided and found."
        return -1
    __log.info("Corpus paths: " + str(corpus_paths))
    __log.debug("Start Turney experiment")
    
    questions = Question.from_file(conf.get(DEFAULTSECT, "questions"), False)
    __log.info("Number of questions: %d" %len(questions))
    if not questions:
        raise ValueError("Empty question set, please check configuration.")
    
    words = __extract_words(questions)
    __log.info("Number of target words in questions: %d" %len(words))
    find_candidate_rows(corpus_paths, words, 
                        int(conf.get(DEFAULTSECT, "candidate_row_min_length")), 
                        int(conf.get(DEFAULTSECT, "candidate_row_min_count")),
                        conf.get(DEFAULTSECT, "candidate_row_path"))
    __log.info("Number of candidate rows: %d" %len(words))
    
    domain_contexts = find_contexts_sequential(spaces.domain_coocurrences,
                                                    corpus_paths, words, 50000, 
                                                    conf.get(DEFAULTSECT, "domain-contexts"))
    count_coocurrences_sequential(spaces.domain_coocurrences,
                                  corpus_paths, words, domain_contexts, 
                                  conf.get(DEFAULTSECT, "raw-domain-space"))
    run_turney_on_space(questions, "domain",
                        conf.get(DEFAULTSECT, "raw-domain-space"), 
                        conf.get(DEFAULTSECT, "domain-contexts"), 
                        conf.get(DEFAULTSECT, "processed-domain-space"))


def run_turney_parallel(conf):
    max_processes = int(conf.get(DEFAULTSECT, "max_processes"))
    if max_processes < 1:
        print "Number of processes must be at least one."
        return -1
    corpus_paths = __convert_paths(conf.get(DEFAULTSECT, "corpora"))
    if len(corpus_paths) <= 0:
        print "No corpus path is provided and found."
        return -1
    sort_by_size(corpus_paths, -1)
    __log.info("Corpus paths: " + str(corpus_paths))
    __log.info("File sizes: " + 
               str([os.stat(path).st_size for path in corpus_paths]))
    __log.debug("Start Turney experiment")
    
    questions = Question.from_file(conf.get(DEFAULTSECT, "questions"), False)
    __log.info("Number of questions: %d" %len(questions))
    if not questions:
        raise ValueError("Empty question set, please check configuration.")
    
    words = __extract_words(questions)
    __log.info("Number of target words in questions: %d" %len(words))
    find_candidate_rows(corpus_paths, words,  
                        int(conf.get(DEFAULTSECT, "candidate_row_min_length")), 
                        int(conf.get(DEFAULTSECT, "candidate_row_min_count")),
                        conf.get(DEFAULTSECT, "candidate_row_path"))
    __log.info("Number of candidate rows: %d" %len(words))
    
    context_threshold = 0
    if conf.has_option(DEFAULTSECT, 'context-threshold'):
        context_threshold = int(conf.get(DEFAULTSECT, "context-threshold"))
    if context_threshold > 0 and __log.isEnabledFor(DEBUG):
        __log.debug("Context frequency threshold used: %d "
                    "(may lead to incorrect result)." %context_threshold)
    
    domain_contexts = find_contexts_by_merging(spaces.domain_coocurrences,
                                                    corpus_paths, words, 50000, 
                                                    conf.get(DEFAULTSECT, "domain-contexts"),
                                                    context_threshold, max_processes)
    count_coocurrences_by_merging(spaces.domain_coocurrences,
                                  corpus_paths, words, domain_contexts, 
                                  conf.get(DEFAULTSECT, "raw-domain-space"),
                                  max_processes)
    run_turney_on_space(questions, "domain",
                        conf.get(DEFAULTSECT, "raw-domain-space"), 
                        conf.get(DEFAULTSECT, "domain-contexts"), 
                        conf.get(DEFAULTSECT, "processed-domain-space"))


def find_candidate_rows(corpus_paths, words, min_length, min_count, 
                        result_path=None):
    '''
    Add all words that exceed thresholds to a set of words.
    '''
    if result_path and os.path.exists(result_path):
        __log.debug("Candidate rows are read from %s" %result_path)
        words.update(read_stripped_lines(result_path))
    else:
        __log.debug("Finding candidate rows...")
        counter = Counter()
        sentence_count = 0
        for sentence in ukwac.sentences(corpus_paths):
            try:
                for node in ukwac.dependency_nodes(sentence, False):
                    if len(node.word) >= min_length:
                        counter[ukwac.node2str(node)] += 1
            except:
                __log.exception("Exception while finding candidate rows.")
            sentence_count += 1
            if sentence_count % 100000 == 0:
                __log.debug("#sentences=%d, #words=%d" %(sentence_count, len(counter)))
        candidates = [key for key in counter if counter[key] >= min_count]
        words.update(candidates)
        if result_path:
            __log.debug("Writing candiate rows into %s" %result_path)
            with open(result_path, "wt") as f:
                for candidate in candidates:
                    f.write(candidate)
                    f.write("\n")


def sort_by_size(paths, order=1):
    paths.sort(key=lambda path: order * os.stat(path).st_size)


if __name__ == '__main__':
    if len(sys.argv) > 2:
        print "Usage: python experiment.py [config-path]"
        sys.exit(1)
        
    conf = ConfigParser()
    conf_path = 'ex42.cfg'
    if len(sys.argv) == 2:
        conf_path = sys.argv[1]
    fileConfig(conf_path) # config logging
    conf.read(conf_path)
    
    if conf.has_option(DEFAULTSECT, 'output-duplicate-path'):
        odpath = conf.get(DEFAULTSECT, 'output-duplicate-path')
        tee = subprocess.Popen(["tee", odpath], 
                               stdin=subprocess.PIPE)
        os.dup2(tee.stdin.fileno(), sys.stdout.fileno())
        os.dup2(tee.stdin.fileno(), sys.stderr.fileno())
    
    mode = conf.get(DEFAULTSECT, "mode")
    if mode == "parallel":
        run_turney_parallel(conf)
    elif mode == "sequential":
        run_turney_sequential(conf)
    else:
        sys.stderr.write("Unknown mode: %s." %mode);
        sys.exit(1)

