# -*- coding: utf-8 -*-
'''
Created on Mar 12, 2013

@author: Lê Ngọc Minh
'''

from _abcoll import Mapping
from bintrees import FastRBTree
from logging import DEBUG
from operator import itemgetter as _itemgetter
from threading import Thread, Lock
import heapq
import logging
import subprocess
import time
import os
import locale


_log = logging.getLogger('ex42.data')
    
    
def read_counts(path):
    with open(path) as f:
        return [parse_count(line) for line in f]


def parse_count(line):
    item = line.split("\t")
    if len(item) > 2: 
        key = item[:-1]
    else:
        key = item[0]
    count = int(item[-1])
    return (key, count)


def write_counter_key_value(f, key, value):
    if isinstance(key, basestring):
        f.write(key)
    else:
        f.write("\t".join(key))
    f.write("\t%d\n" %value)


def write_counter(counter, path):
    """
    Designed to pair with merge function.
    """
    original_locale = os.environ.get("LC_ALL", "")
    try:
        os.environ["LC_ALL"] = "C"
        args = ["sort", "-o", path];
        p = subprocess.Popen(args, stdin=subprocess.PIPE)
        with p.stdin:
            for key in counter:
                write_counter_key_value(p.stdin, key, counter[key])
        p.wait()
    finally:
        os.environ["LC_ALL"] = original_locale


def sort_and_write_all(counter, path):
    keys = sorted(counter, key=lambda(a): a[0])
    with open(path, "wt") as f:
        for key in keys:
            write_counter_key_value(f, key, counter[key])


def sort_and_write_most_commons(counter, n, path):
    items = sorted(counter.most_common(n), key=lambda(a): a[0])
    with open(path, "wt") as f:
        for item in items:
            write_counter_key_value(f, item[0], item[1])


def merge(dest, paths):
    """
    Merge different count files to a single count file using sort command
    in merge mode. Input files must be created with write_counter function.
    """
    if isinstance(paths, basestring):
        paths = [paths]
    original_locale = os.environ.get("LC_ALL", "")
    try:
        os.environ["LC_ALL"] = "C"
        args = ["sort", "-m"];
        args.extend(paths)
        p = subprocess.Popen(args, stdout=subprocess.PIPE)
        previous_key = None
        previous_count = 0
        with open(dest, "w") as f:
            for line in p.stdout:
                key, count = parse_count(line)
                if key == previous_key:
                    previous_count += int(count)
                else:
                    if previous_key is not None:
                        write_counter_key_value(f, previous_key, previous_count)
                    previous_key = key
                    previous_count = int(count)
            if previous_key is not None: # write the last one
                write_counter_key_value(f, previous_key, previous_count)
    finally:
        os.environ["LC_ALL"] = original_locale


class AsynchronousCounter:
    """
    A counter that provides an asynchronous update function
    """

    def __init__(self, counter):
        self.counter = counter
        self.threads = []
        self.lock = Lock()

        
    def __getitem__(self, k):
        return self.counter[k]
    
    
    def __setitem__(self, k, v):
        self.counter[k] = v
        
        
    def __len__(self):
        return self.counter.__len__()
    
    
    def __iter__(self):
        return self.counter.__iter__()
    

    def update_locked(self, counter):
        if self.lock.acquire():
            try:
                start = time.clock()
                self.counter.update(counter)
                elapsed = time.clock() - start
                if _log.isEnabledFor(DEBUG):
                    _log.debug("Completed merging %d keys in %f seconds."
                                %(len(counter), elapsed))
            finally:
                self.lock.release()


    def update_async(self, counter):
        thread = Thread(target=self.update_locked, args=(counter,))
        thread.start()
        self.threads.append(thread)
        
        
    def wait(self):
        for thread in self.threads:
            thread.join()


    def most_common(self, n):
        return self.counter.most_common(n)
        
            
class TreeCounter:
    """
    An attempt to replace hash table with tree. Sadly it is 20 times 
    slower than normal hash table (see data_test module).
    @deprecated: tooooo slow
    """
    
    def __init__(self):
        self.tree = FastRBTree()
        

    def get(self, k, d):
        return self.tree.get(k, d)

        
    def __getitem__(self, x):
        return self.tree.get(x, 0)
    
    
    def __setitem__(self, k, v):
        self.tree[k] = v
    
    
    def iteritems(self):
        return self.tree.items()
    
    
    def update(self, iterable):
        if iterable is not None:
            if isinstance(iterable, Mapping):
                self_get = self.tree.get
                for elem, count in iterable.iteritems():
                    self[elem] = self_get(elem, 0) + count
            else:
                self_get = self.tree.get
                for elem in iterable:
                    self[elem] = self_get(elem, 0) + 1


    def most_common(self, n):
        if n is None:
            return sorted(self.iteritems(), key=_itemgetter(1), reverse=True)
        return heapq.nlargest(n, self.iteritems(), key=_itemgetter(1))
    