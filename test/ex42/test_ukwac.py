# -*- coding: utf-8 -*-
'''
Created on Feb 17, 2013

@author: Lê Ngọc Minh
'''
from ex42 import ukwac
from ex42.ukwac import _roulette_binary_search, _roulette
import os
import shutil
import unittest

class Test(unittest.TestCase):

    def test_sentences_empty(self): 
        self.assertRaises(StopIteration, 
                          ukwac.sentences('test-data/empty.xml').next)


    def test_no_relation(self):
        with ukwac.SentenceIterator('test-data/ukwac.1.xml') as it:
            sentence = it.next()
            nodes = ukwac.dependency_nodes(sentence, False)
            for node in nodes:
                self.assertEquals(0, len(node.relations))
                self.assertIsNone(node.find_parent())


    def test_sentences(self):
        sentences = list(ukwac.sentences('test-data/ukwac.1000.xml'))
        self.assertEqual(10, len(sentences[0].splitlines()))
        self.assertEqual(35, len(sentences))


    def test_illformed_data(self):
        sentences = list(ukwac.sentences('test-data/ukwac.illformed.xml'))
        self.assertEqual(1, len(sentences))


    def test_gzip(self):
        sentences = list(ukwac.sentences('test-data/ukwac.1000.xml.gz'))
        self.assertEqual(10, len(sentences[0].splitlines()))
        self.assertEqual(35, len(sentences))
        
        
    def test_dependency_tree_node_str(self):
        node = ukwac.DependencyTreeNode("tested", "V", "test", 1)
        print node
        
        
    def test_dependency_nodes(self):
        with ukwac.SentenceIterator('test-data/ukwac.1000.xml') as reader:
            sentence = next(iter(reader))
            deps = ukwac.dependency_nodes(sentence)
            word0 = deps[0]
            self.assertEqual("Hooligans", word0.word)
            self.assertEqual(1, len(word0.relations))
            self.assertEqual("passion", word0.find_parent().word)


    def test_dependency_coocurrences(self):
        coocurrences = ukwac.dependency_coocurrences('test-data/ukwac.1000.xml')
        print len(coocurrences)
        print coocurrences.items()[:10]
        
    
    def test_random_split(self):
        shutil.rmtree("target/split1", True)
        os.makedirs("target/split1")
        print ukwac.random_split("test-data/ukwac.100k.xml.gz", 
                                 "target/split1/split-%02d.xml",
                                 n=10, min_priority=0.8, gzip=False)
        
        shutil.rmtree("target/split2", True)
        os.makedirs("target/split2")
        print ukwac.random_split("test-data/ukwac.100k.xml.gz", 
                                 "target/split2/split-%02d.xml", 
                                 n=10, min_priority=0.8, gzip=True)
        
        
    def test_random_split_with_globbing(self):
        shutil.rmtree("target/split-gz", True)
        os.makedirs("target/split-gz")
        ukwac.random_split(["test-data/ukwac.10*.xml.gz", "ukwac.1.xml"], 
                           "target/split-gz/split-glob-%02d.xml",
                           n=10, min_priority=0.8, gzip=True)


    def asserRoulette(self, roulette, n):
        self.assertEqual(n, len(roulette))
        self.assertEqual(0, roulette[0])
        for i in range(len(roulette)-1):
            self.assertLessEqual(roulette[i], roulette[i+1])
        self.assertLessEqual(roulette[-1], 1)


    def test_roulette(self):
        self.asserRoulette(_roulette(1, 1, 1), 1)
        self.asserRoulette(_roulette(1, 0, 1), 1)
        self.asserRoulette(_roulette(100, 1, 1), 100)
        self.asserRoulette(_roulette(100, 0, 1), 100)
        self.asserRoulette(_roulette(100, 0.5, 1), 100)
    

    def test_roulette_binary_search(self):
        self.assertEqual(0, _roulette_binary_search([0], 0.5))
        self.assertEqual(0, _roulette_binary_search([0, 1], 0.5))
        self.assertEqual(1, _roulette_binary_search([0, 0.6], 1))
        self.assertEqual(1, _roulette_binary_search([0, 0.6], 0.7))
        self.assertEqual(0, _roulette_binary_search([0, 0.6], 0.1))
        self.assertEqual(0, _roulette_binary_search([0, 0.6], 0))
        self.assertEqual(1, _roulette_binary_search([0, 0.6, 0.7], 0.6))
        self.assertEqual(1, _roulette_binary_search([0, 0.6, 0.7], 0.65))
        self.assertEqual(2, _roulette_binary_search([0, 0.6, 0.7], 1))
        

if __name__ == "__main__":
    import sys;sys.argv = ['', 
                           'Test.test_roulette'
                           ]
    unittest.main()