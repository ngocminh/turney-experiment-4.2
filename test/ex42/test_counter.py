# -*- coding: utf-8 -*-
'''
Created on Mar 7, 2013

@author: Lê Ngọc Minh
'''
import unittest
from collections import Counter
from ex42.spaces import domain_coocurrences
import time
from glob import glob
from multiprocessing.pool import Pool
from multiprocessing import cpu_count
from IN import INT_MAX
from ex42 import spaces
from ex42.experiment import _count_contexts, _count_coocurrences, sort_by_size
from os import path
import os
import traceback
import multiprocessing
from logging import DEBUG
import logging


__log = logging.getLogger('ex42')
multiprocessing.log_to_stderr()


def _time_it(f, *args):
    start = time.clock()
    f(*args)
    return (time.clock() - start)*1000


def _read_and_count(corpus_paths, max_sentences):
    counter = Counter()
    if max_sentences <= 0:
        return counter
    
    start = time.clock()
    counter._sentence_count = 0
    def increase_counter(c): 
        counter[c[1]] += 1
        counter._sentence_count += 1
        if counter._sentence_count > max_sentences:
            raise StopIteration
    domain_coocurrences(corpus_paths, increase_counter)
    
    if __log.isEnabledFor(DEBUG):
        elapsed = (time.clock() - start)*1000
        size = 0
        if isinstance(corpus_paths, basestring):
            corpus_paths = (corpus_paths,)
        for path in corpus_paths:
            size += os.stat(path).st_size
        __log.debug("Finished processing %s (size: %d) after %f"
                    %(corpus_paths, size, elapsed))
    return counter
    
    
def _read_and_count_cooccurrences(paths, max_sentences):
    return _count_coocurrences(spaces.domain_coocurrences, paths, None, None,
                               multiprocessing=True)
    
    
def _read_and_count_contexts(self, paths, max_sentences):
    return _count_contexts(spaces.domain_coocurrences, paths, None,
                           multiprocessing=True)
    

class Test(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        cls.corpus_paths = glob('test-data/split/*.xml.gz')
        #cls.big_corpus_paths = glob('test-data/split-big/*.xml.gz')
        cls.big_corpus_paths = glob('test-data/split/*.xml.gz')

    
    def _read_and_count_in_parallel(self, corpus_paths, max_processes):
        counter = Counter()
        pool = Pool(max_processes)
        for i in range(len(corpus_paths)):
#            counter.update(_read_and_count(corpus_paths[i]))
            pool.apply_async(_read_and_count, 
                             args=(corpus_paths[i], INT_MAX),
                             callback=counter.update)
        pool.close()
        pool.join()
        self.assertTrue(len(counter) > 0, "Counter is empty")
    

    def test_read_and_count_performance(self):
        '''
        Measure the increase of running time when we add more sentences. This
        includes the time of reading from disk and adding to dictionary. 
        '''
        print("#lines\ttime (ticks)")
        for i in range(0, 1000000, 20000):
            time = _time_it(_read_and_count, Test.corpus_paths, i)
            print("%d\t%d" %(i, time))
        
        
    def test_update_performance(self):
        '''
split$ zcat ukwac000-02.xml.gz | grep "<s>" | wc -l
6873
split$ zcat ukwac000-01.xml.gz | grep "<s>" | wc -l
6992
split$ zcat ukwac000-03.xml.gz | grep "<s>" | wc -l
6669
split$ zcat ukwac000-00.xml.gz | grep "<s>" | wc -l
7048
        '''
        corpus_paths = glob('test-data/split/ukwac000-*.xml.gz')
        print("#lines\ttime (ticks)")
        for i in range(2000, 20001, 2000):
            counter1 = _read_and_count(corpus_paths, i)
            counter2 = _read_and_count(corpus_paths, i)
            self.assertGreater(len(counter1), 0)
            self.assertGreater(len(counter2), 0)
            time = _time_it(counter1.update, counter2)
            print("%d\t%d" %(i, time))


    def test_longest_file_first(self):
        linear_paths = glob('test-data/split-linear-small/*.xml.gz')
        equal_paths = glob('test-data/split-equal-small/*.xml.gz')
        self.assertTrue(len(linear_paths) > 0, "No file found")
        self.assertTrue(len(equal_paths) > 0, "No file found")
        max_processes = 5

        equal_parts_time = _time_it(self._read_and_count_in_parallel, 
                                    equal_paths, max_processes)
        print("Equal-parts time: %d" %equal_parts_time)
        
        sort_by_size(linear_paths, 1)
        print [os.stat(path).st_size for path in linear_paths]
        self.assertLess(os.stat(linear_paths[0]).st_size, 
                        os.stat(linear_paths[1]).st_size)
        asc_time = _time_it(self._read_and_count_in_parallel, 
                            linear_paths, max_processes)
        print("Ascending time: %d" %asc_time)
        #self.assertGreater(asc_time, equal_parts_time) 
        
        sort_by_size(linear_paths, -1)
        print [os.stat(path).st_size for path in linear_paths]
        self.assertGreater(os.stat(linear_paths[0]).st_size, 
                        os.stat(linear_paths[1]).st_size)
        desc_time = _time_it(self._read_and_count_in_parallel, 
                             linear_paths, max_processes)
#        self.assertLess(desc_time, equal_parts_time) 
        print("Descending time: %d" %desc_time)


    def run_on_processes(self, corpus_func, corpus_paths, max_processes):
        # let's assume that we have many more paths than CPUs
        counter = Counter()
        path_count = cpu_count() * 3 
        pool = Pool(max_processes)
        for i in range(path_count):
            path = corpus_paths[i % len(corpus_paths)]
            #counter.update(corpus_func(path, INT_MAX))
            pool.apply_async(corpus_func, args=(path, INT_MAX), 
                             callback=counter.update)
        pool.close()
        pool.join()
        self.assertTrue(len(counter) > 0, "Counter is empty")


    def run_on_varied_numbers_of_processes(self, f, corpus_paths):
        print "max(#processes)\ttime"
        for max_processes in range(2, cpu_count()*2+1):
            time = _time_it(self.run_on_processes, 
                            f, corpus_paths, max_processes);
            print "%d\t%d" %(max_processes, time)
        
        
    def big_test_number_of_processes(self):
        '''
        Try to find the number of processes that maximizes throughput.
        '''
        self.assertTrue(len(Test.big_corpus_paths) > 0, "Big test needs big data")
        self.run_on_varied_numbers_of_processes(_read_and_count, Test.big_corpus_paths)
        
        
    def big_test_number_of_processes_for_counting_contexts(self):
        '''
        Try to find the number of processes that maximizes throughput.
        '''
        self.assertTrue(len(Test.big_corpus_paths) > 0, "Big test needs big data")
        self.run_on_varied_numbers_of_processes(_read_and_count_contexts, 
                                                Test.big_corpus_paths)
        
        
    def big_test_number_of_processes_for_counting_cooccurrences(self):
        '''
        Try to find the number of processes that maximizes throughput.
        '''
        self.assertTrue(len(Test.big_corpus_paths) > 0, "Big test needs big data")
        self.run_on_varied_numbers_of_processes(_read_and_count_cooccurrences, 
                                                Test.big_corpus_paths)


if __name__ == "__main__":
    import sys;sys.argv = ['', 
                           'Test.test_longest_file_first',
                           'Test.big_test_number_of_processes',
                           'Test.big_test_number_of_processes_for_counting_contexts',
                           'Test.big_test_number_of_processes_for_counting_cooccurrences',
                           ]
    unittest.main()