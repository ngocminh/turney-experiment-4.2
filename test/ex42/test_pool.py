# -*- coding: utf-8 -*-
'''
Created on Mar 8, 2013

@author: Lê Ngọc Minh
'''
import unittest
from multiprocessing.pool import Pool
from multiprocessing import log_to_stderr
import multiprocessing
import traceback


log_to_stderr()


# Shortcut to multiprocessing's logger
def error(msg, *args):
    return multiprocessing.get_logger().error(msg, *args)


class LogExceptions(object):
    def __init__(self, callable):
        self.__callable = callable
        return

    def __call__(self, *args, **kwargs):
        try:
            result = self.__callable(*args, **kwargs)

        except Exception as e:
            # Here we add some debugging help. If multiprocessing's
            # debugging is on, it will arrange to log the traceback
            error(traceback.format_exc())
            # Re-raise the original exception so the Pool worker can
            # clean up
            raise

        # It was fine, give a normal answer
        return result
    pass

class LoggingPool(Pool):
    def apply_async(self, func, args=(), kwds={}, callback=None):
        return Pool.apply_async(self, LogExceptions(func), args, kwds, callback)


def f(a, b):
    print a
    print b


class Test(unittest.TestCase):


    def test_missing_argument(self):
        pool = LoggingPool(2)
        for i in range(4):
            pool.apply_async(f, args=(i))
        pool.close()
        pool.join()


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.test_missing_argument']
    unittest.main()