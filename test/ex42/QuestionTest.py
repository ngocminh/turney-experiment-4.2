# -*- coding: utf-8 -*-
'''
Created on Feb 13, 2013

@author: Lê Ngọc Minh
'''
from ex42 import Question
import unittest

class QuestionTest(unittest.TestCase):

    def test_of(self):
        spec = "voiceless-j\tconsonant-n\tsurd-n\tunvoiced-j\tphone-n\tamorality-n\tsalp-n"
        self.assertEqual(7, len(spec.split("\t")))

        question = Question.of(spec)
        self.assertEqual("voiceless-j", question.modifier)
        self.assertEqual("consonant-n", question.head)
        self.assertEqual("surd-n", question.correct_answer)
        self.assertEqual(4, len(question.foils))


    def test_choices(self):
        spec = "voiceless-j\tconsonant-n\tsurd-n\tunvoiced-j\tphone-n\tamorality-n\tsalp-n"
        question = Question.of(spec)
        self.assertEqual(7, len(question.choices))


    def test_from_file(self):
        Question.from_file("test-data/questions.10.lst", False)
        

if __name__ == '__main__':
    unittest.main()

