# -*- coding: utf-8 -*-
'''
Created on Feb 18, 2013

@author: Lê Ngọc Minh
'''
from ConfigParser import ConfigParser
from ex42 import experiment, spaces
from ex42.experiment import read_stripped_lines, \
    find_contexts_sequential, count_coocurrences_sequential,\
    find_contexts_by_merging, count_coocurrences_by_merging,\
    _read_most_common_contexts
import os
import shutil
import unittest
from logging.config import fileConfig
from IN import INT_MAX


class Test(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        conf_path = "test-data/ex42.cfg"
        cls.conf = ConfigParser()
        cls.conf.read(conf_path)
        fileConfig(conf_path)
        
        cls.words = set(read_stripped_lines('test-data/corpus_noun.lst'))
        cls.contexts = set(['executive', 'tables', 'hospitality' , 'portsmouth', 
                            'old', 'takeover', 'league', 'club', 'football', 
                            'premier', 'fans', 'hilaire', 'pompey', 'boxes', 
                            'united', 'passion', 'team', 'leeds', 'cities', 
                            'trafford']);
                            
        cls.small_paths = ["test-data/split-small/ukwac000-00-00.xml.gz",
                           "test-data/split-small/ukwac000-00-01.xml.gz",
                           "test-data/split-small/ukwac000-00-02.xml.gz",
                           "test-data/split-small/ukwac000-00-03.xml.gz"]
        cls.small_contexts = set(read_stripped_lines('test-data/split-small/context.small.lst'));
                            
        cls.big_paths = ["test-data/split/ukwac000-00.xml.gz",
                         "test-data/split/ukwac000-01.xml.gz",
                         "test-data/split/ukwac000-02.xml.gz",
                         "test-data/split/ukwac000-03.xml.gz"]
        cls.big_contexts = set(read_stripped_lines('test-data/split/context.big.lst'));
        

    def setUp(self):
        shutil.rmtree("target")
        os.mkdir("target")
        
        
    def test_read_stripped_lines(self):
        lines = list(read_stripped_lines('test-data/corpus_noun.lst'))
        self.assertTrue(len(lines) > 0)
        for line in lines:
            self.assertFalse(line.endswith(' '))
            self.assertFalse(line.endswith('\t'))
            self.assertFalse(line.endswith('\n'))
        
        
    def test_read_most_common_contexts(self):
        contexts = set(_read_most_common_contexts("test-data/counts5", 4))
        self.assertTrue(("man", 1200) in contexts)
        self.assertTrue(("woman", 1200) in contexts)
        self.assertTrue(("car", 200) in contexts)
        self.assertTrue(("building", 100) in contexts)
        self.assertEqual(4, len(contexts))

    
    def test_find_contexts_by_merging(self):
        contexts = find_contexts_by_merging(spaces.domain_coocurrences,
                                            ["test-data/ukwac.1000.xml"]*4, 
                                            Test.words, 10, 
                                           "target/context.merge.lst");
        print contexts
        self.assertEqual(10, len(contexts))
        self.assertTrue(os.path.exists("target/context.merge.lst"))


    def test_find_contexts_small_with_threashold(self):
        n = 1000
        threshold = 1
        
        contexts2 = find_contexts_by_merging(spaces.domain_coocurrences,
                                             Test.small_paths, None, n, 
                                             "target/context.threshold.lst",
                                             threshold);
        self.assertSetPrecision(contexts2, Test.small_contexts, 0.98)
    
        
    def test_find_contexts_exception_in_process(self):
        path = "test-data/ukwac.with-error.xml"
        # test that it terminates without error
        contexts = find_contexts_by_merging(spaces.domain_coocurrences,
                                            path, None, INT_MAX, 
                                            "target/context.exception.lst");
        # and can still read well-formed data
        self.assertTrue(len(contexts) > 0, 
                        "No context found due to error in input data!")

    
    def test_find_contexts_sequential(self):
        contexts = find_contexts_sequential(spaces.domain_coocurrences,
                                            "test-data/ukwac.1000.xml", 
                                            Test.words, 10, 
                                            "target/context.sequential.lst");
        print contexts
        self.assertEqual(10, len(contexts))
        self.assertTrue(os.path.exists("target/context.sequential.lst"))
        
        
    def test_find_contexts_exception_sequential(self):
        path = "test-data/ukwac.with-error.xml"
        # test that it terminates without error
        contexts = find_contexts_sequential(spaces.domain_coocurrences,
                                            path, None, INT_MAX, None);
        # and can still read well-formed data
        self.assertTrue(len(contexts) > 0, 
                        "No context found due to error in input data!")
        
        
    def assertSetPrecision(self, measuredSet, referenceSet, expectedPrecision):
        wrongItems = []
        count = 0
        for item in measuredSet:
            if item in referenceSet:
                count += 1
            else:
                wrongItems.append(item)
        precision = count / float(len(measuredSet))
        print "precision: %.2f%%" %(precision*100)
        print "Wrong items:%s\n" %", ".join(wrongItems)
        self.assertGreaterEqual(precision, expectedPrecision, 
                                "Precision is %f, lower than expected %f."
                                %(precision, expectedPrecision));


    def test_count_coocurrences_by_merging(self):
        count_coocurrences_by_merging(spaces.domain_coocurrences,
                                      Test.small_paths, None, 
                                      Test.small_contexts, 'target/cooccurrences-small.merge.lst')


    def test_count_coocurrences_exception_in_process(self):
        path = "test-data/ukwac.with-error.xml"
        result = "target/count.exception.sm"
        count_coocurrences_by_merging(spaces.domain_coocurrences,
                                      path, None, Test.big_contexts, result)
        with open(result) as f:
            self.assertTrue(len(f.readlines()) > 0, 
                            "No co-occurrence found due to error in input data!")        


    def test_count_coocurrences_sequential(self):
        count_coocurrences_sequential(spaces.domain_coocurrences,
                                      Test.small_paths, None, 
                                      Test.small_contexts, 'target/cooccurrences-small.lst')


    def test_count_coocurrences_exception_sequential(self):
        path = "test-data/ukwac.with-error.xml"
        counter = count_coocurrences_sequential(spaces.domain_coocurrences,
                                                path, None, Test.big_contexts, None)
        self.assertTrue(len(counter)>0, 
                        "No co-occurrence found due to error in input data!")        


    def test_find_candidate_rows_no_output(self):
        words = set()
        experiment.find_candidate_rows("test-data/ukwac.100k.xml.gz", words, 
                                       2, 10, None)
        self.assertGreater(len(words), 0)


    def test_find_candidate_rows_with_output(self):
        words = set()
        experiment.find_candidate_rows("test-data/ukwac.100k.xml.gz", words, 
                                       2, 10, "target/candiate-row.ukwac-100k.txt")
        self.assertGreater(len(words), 0)


    def test_run_turney_sequential(self):
        experiment.run_turney_sequential(Test.conf)
        

    def test_run_turney_parallel(self):
        experiment.run_turney_parallel(Test.conf)
        
        
    def big_test_find_contexts_no_threashold(self):
        '''
        Test finding contexts on big data (nearer to realistic data) in the 
        hope to find some problems. The name has been changed to avoid 
        automatic run of this method.
        '''
        paths = ["test-data/split/ukwac000-00.xml.gz",
                 "test-data/split/ukwac000-01.xml.gz",
                 "test-data/split/ukwac000-02.xml.gz",
                 "test-data/split/ukwac000-03.xml.gz"]
        n = 1000
        
        contexts1 = set(read_stripped_lines('test-data/split/context.big.lst'));
        self.assertEqual(n, len(contexts1)) 
                                                       
        contexts2 = find_contexts_by_merging(spaces.domain_coocurrences,
                                             paths, None, n, None);
        self.assertSetEqual(contexts1, contexts2)
        
        
    def big_test_find_contexts_with_threashold(self):
        '''
        Test finding contexts on big data (nearer to realistic data) in the 
        hope to find some problems. The name has been changed to avoid 
        automatic run of this method.
        '''
        n = 1000
        threshold = 20
        
        contexts2 = find_contexts_by_merging(spaces.domain_coocurrences,
                                             Test.big_paths, None, n, 
                                             None, threshold);
        self.assertSetPrecision(contexts2, Test.big_contexts, 0.99)


if __name__ == "__main__":
    import sys;sys.argv = ['',
                           'Test.test_count_coocurrences_exception_in_process',
                           'Test.test_find_contexts_exception_in_process', 
                           ]
    unittest.main()