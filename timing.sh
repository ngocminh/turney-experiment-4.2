EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` log-or-output-file"
  exit $E_BADARGS
fi

grep -h -E "Start|Finish" $1 | sort
